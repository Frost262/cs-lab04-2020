#include <iostream>
#include <vector>
#include "histogram.h"
#include <curl/curl.h>
//#include <curl/easy.h>
#include <sstream>
#include <string>
#include <windows.h>

using namespace std;



vector<double>
input_numbers(istream & in, size_t count)
{
    vector<double> result(count);
    for (size_t i = 0; i < count; i++)
    {
        in >> result[i];
    }
    return result;
}

Input read_input (istream& in, bool prompt)
{
    Input data;
    if (prompt)
        cerr<< "Enter number count";
    size_t number_count;
    in >> number_count;

    if (prompt)
        cerr << "Enter numbers: ";
    data.numbers = input_numbers(in, number_count);

    //size_t bin_count;
    if (prompt)
        cerr << "Enter column count: ";
    in >> data.bin_count;

    return data;
}


vector<size_t>
make_historgam (const Input& data)
{
    double min;
    double max;
    vector<size_t> bins(data.bin_count);
    find_minmax(data.numbers, min, max);
    for (double number : data.numbers)
    {
        size_t bin = (size_t)((number - min) / (max - min) * data.bin_count);
        if (bin == data.bin_count)
        {
            bin--;
        }
        bins[bin]++;
    }
    return bins;
}

void
show_histogram_text (vector<size_t> bins)
{
// ����� ������
    const size_t SCREEN_WIDTH = 80;
    const size_t MAX_ASTERISK = SCREEN_WIDTH - 4 - 1;

    size_t max_count = 0;
    for (size_t count : bins)
    {
        if (count > max_count)
        {
            max_count = count;
        }
    }
    const bool scaling_needed = max_count > MAX_ASTERISK;

    for (size_t bin : bins)
    {
        if (bin < 100)
        {
            cout << ' ';
        }
        if (bin < 10)
        {
            cout << ' ';
        }
        cout << bin << "|";

        size_t height = bin;
        if (scaling_needed)
        {
            const double scaling_factor = (double)MAX_ASTERISK / max_count;
            height = (size_t)(bin * scaling_factor);
        }

        for (size_t i = 0; i < height; i++)
        {
            cout << '*';
        }
        cout << '\n';
    }
}

void
svg_begin(double width, double height)
{
    cout << "<?xml version='1.0' encoding='UTF-8'?>\n";
    cout << "<svg ";
    cout << "width='" << width << "' ";
    cout << "height='" << height << "' ";
    cout << "viewBox='0 0 " << width << " " << height << "' ";
    cout << "xmlns='http://www.w3.org/2000/svg'>\n";
}

void
svg_end()
{
    cout << "</svg>\n";
}


void svg_text(double left, double baseline, string text)
{
    cout << "<text x='"<<left<<"' y='"<<baseline<<"'>"<<text<<"</text>";
}



void svg_line(double x1, double y1, double x2, double y2, string stroke = "black", int strokewidth = 1)
{
    cout << "<line x1='"<<x1<<"' y1='"<<y1<<"' x2='"<<x2<<"' y2='"<<y2<<"' stroke='"<<stroke<<"' stroke-width='"<<strokewidth<<"' stroke-dasharray = '10 10' />";
}


void
show_histogram_svg(const vector<size_t>& bins)
{
    const auto IMAGE_WIDTH = 400;
    const auto IMAGE_HEIGHT = 300;
    const auto TEXT_LEFT = 20;
    const auto TEXT_BASELINE = 20;
    const auto TEXT_WIDTH = 50;
    const auto BIN_HEIGHT = 30;

    svg_begin(IMAGE_WIDTH, IMAGE_HEIGHT);
    /*svg_text(TEXT_LEFT,TEXT_BASELINE,to_string(bins[0]));
    svg_rect(TEXT_WIDTH, 0, bins[0] * 10, BIN_HEIGHT);*/
    double top = 0;
    int i = 0;
    for (size_t bin : bins)
    {
        i++;
        const double bin_width = 10 * bin;
        svg_text(TEXT_LEFT, top + TEXT_BASELINE, to_string(bin));
        svg_rect(TEXT_WIDTH, top, bin_width, BIN_HEIGHT, "black","#111");
        /*svg_text(TEXT_LEFT/2, top + TEXT_BASELINE/4, to_string(printf("%.2f",1.33*i)));*/
        //svg_line(TEXT_WIDTH - 20, top+BIN_HEIGHT, TEXT_WIDTH+bin_width + 1000, top+BIN_HEIGHT);
        //svg_text(TEXT_LEFT + bin_width+40, top + TEXT_BASELINE, "33%");
        top += BIN_HEIGHT;
    }
    svg_end();
}
size_t
write_data(void* items, size_t item_size, size_t item_count, void* ctx) {
    // TODO: ���������� ������ � ������.
    stringstream* buffer = reinterpret_cast<stringstream*>(ctx);
    size_t data_size = item_size * item_count;
    const char* items_ = reinterpret_cast<char*>(items);
    buffer->write (items_, data_size);
    return data_size;
}


Input
download(const string& address)
{
    stringstream buffer;

    CURL* curl = curl_easy_init();
        if(curl)
        {
            CURLcode res;
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
            curl_easy_setopt(curl, CURLOPT_URL, address.c_str());
            res = curl_easy_perform(curl);
            curl_easy_cleanup(curl);
        }

    return read_input(buffer, false);
}

int
main(int argc, char* argv[])
{
   /* if (argc >1) {
        cout << argc<< endl;
        for (int i = 0; i < argc; i++){
            cout << argv [i]<< endl;
        }
        return 0;
    }*/
    Input input;

    if (argc > 1)
    {
        input = download(argv[1]);

    }
    else {
        input = read_input(cin, true);
    }

    /*CURL* curl = curl_easy_init();
        if(curl)
        {
            CURLcode res;
            curl_easy_setopt(curl, CURLOPT_URL, argv[1]);
            res = curl_easy_perform(curl);
            curl_easy_cleanup(curl);
        } */

    // ���� ������
    /*size_t number_count;
    cerr << "Enter number count: ";
    cin >> number_count;

    cerr << "Enter numbers: ";
    //vector<double> numbers(number_count);

    const  auto numbers = input_numbers(cin, number_count);

    size_t bin_count;
    cerr << "Enter column count: ";
    cin >> bin_count;*/
    //Input data;
    //const auto input = read_input(cin, true);

    // ��������� ������
    //double min = numbers[0];
    //double max = numbers[0];

    const auto bins = make_historgam(input);
    //show_histogram_text (bins);
   /* for (size_t inp : input.numbers){
        cout << inp << endl;
    }
    cout << "bins";
    for (size_t bin : bins){
        cout << bin << endl;
    }*/
    show_histogram_svg(bins);

   //cout <<  sizeof(int);

    DWORD info = GetVersion();
    cout << info<< endl;

    DWORD mask = 0x0000ffff;
    DWORD version = info & mask;
    DWORD platform = info >> 16;

    DWORD version_major = version & 0x00ff;
    DWORD version_minor = version >> 8;

    DWORD build;
    if ((info & 0b10000000'00000000'0000000'00000000) == 0) {
         build = platform >> 8 ;

    }


    cout << version_major << "--" << version_minor << "--" << build <<endl;

////
   /*  DWORD dwVersion = 0;
    DWORD dwMajorVersion = 0;
    DWORD dwMinorVersion = 0;
    DWORD dwBuild = 0;

    dwVersion = GetVersion();

    // Get the Windows version.

    dwMajorVersion = (DWORD)(LOBYTE(LOWORD(dwVersion)));
    dwMinorVersion = (DWORD)(HIBYTE(LOWORD(dwVersion)));

    // Get the build number.

    if (dwVersion < 0x80000000)
        dwBuild = (DWORD)(LOBYTE(HIWORD(dwVersion)));

    printf("Version is %d.%d (%d)\n",
                dwMajorVersion,
                dwMinorVersion,
                dwBuild);

*/

char system_dir[MAX_PATH];
GetSystemDirectory(system_dir, MAX_PATH);
printf("System directory: %s", system_dir); // System directory: C:\Windows

char comp_name [MAX_COMPUTERNAME_LENGTH + 1];
DWORD name_size = MAX_COMPUTERNAME_LENGTH + 1;
GetComputerNameA(comp_name, &name_size);
printf("Comp name: %s", comp_name);


    return 0;
}
