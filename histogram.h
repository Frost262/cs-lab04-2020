#ifndef HISTOGRAM_H_INCLUDED
#define HISTOGRAM_H_INCLUDED
#include <vector>
#include <string>
#include <iostream>
using namespace std;

struct Input
{
    vector<double> numbers;
    size_t bin_count;
};


void
find_minmax(const vector<double>& numbers, double& min, double& max) ;

void svg_rect(double x, double y, double width, double height, string stroke , string fill );
#endif // HISTOGRAM_H_INCLUDED
